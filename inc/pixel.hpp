#ifndef PIXEL_HPP
#define PIXEL_HPP

#include <string>
using namespace std;

class Pixel{

//Atributos
protected:
  int ***rgb;
  int **pixels;
//Metodos
public:
  Pixel();
  ~Pixel();

  void setRgb(int ***rgb);
  int*** getRgb();
  
  void setPixels(int **pixels);
  int** getPixels();

};
#endif
