#ifndef IMAGEM_HPP
#define IMAGEM_HPP

#include "pixel.hpp"
#include <string>
using namespace std;

class Imagem : public Pixel {
//Atributos
private:
  string tipo;
  int largura;
  int altura;
  unsigned int escala;
  string nome;


//Metodos
public:

  Imagem();
  ~Imagem();

  void setTipo(string tipo);
    string getTipo();

  void setLargura(int largura);
    int getLargura();

  void setAltura(int altura);
    int getAltura();

  void setEscala(unsigned int escala);
    unsigned int getEscala();

  void setComentario(string comentario);
    string getComentario();

  void leituraImagem();

  void comentarioPGM(string nome);

  void comentarioPPM(string nome);

};

#endif
