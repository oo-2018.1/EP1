#include "pixel.hpp"
#include <fstream>

using namespace std;

Pixel::Pixel(){
  rgb = NULL;
  pixels = NULL;
}

Pixel::~Pixel(){
}

void Pixel::setPixels(int **pixels){
  this -> pixels = pixels;
}
int** Pixel::getPixels(){
  return pixels;
}

void Pixel::setRgb(int ***rgb){
  this -> rgb = rgb;
}
int*** Pixel::getRgb(){
  return rgb;
}

//End
