#include "imagem.hpp"
#include <iostream>
#include <string>
#include <fstream>
#include <string>
#include <string.h>
#include <stdlib.h>
#include <bits/stdc++.h>

using namespace std;

Imagem::Imagem(){
  rgb = 0;
  pixels = 0;
  tipo = "";
  altura = 0;
  largura = 0;
  escala = 0;
  nome = "";

}
Imagem::~Imagem(){
}
void Imagem::setTipo(string tipo){
  this -> tipo = tipo;
}
string Imagem::getTipo(){
  return tipo;
}
void Imagem::setAltura(int altura){
  this -> altura = altura;
}
int Imagem::getAltura(){
  return altura;
}
void Imagem::setLargura(int largura){
  this -> largura = largura;
}
int Imagem::getLargura(){
  return largura;
}
void Imagem::setEscala(unsigned int escala){
  this -> escala = escala;
}
unsigned int Imagem::getEscala(){
  return escala;
}

void Imagem::leituraImagem(){
  //Declaração de variaveis
    char tipo[2] = {'0'};
    int largura = 0;
    int altura = 0;
    int escala = 0;
    int **pixels = NULL;
    int ***rgb;
    int i = 0;
    int j = 0;
    int k = 0;
    char nome_arquivo[100] = {'0'};
    string comentario = "";
    //Modo de leitura da imagem utilizando a biblioteca fsteam;
    ifstream leitura;

    cin >> nome_arquivo;
    leitura.open(nome_arquivo);
    if(!leitura.is_open()){
      leitura.clear();
      throw(0);
    }
    //Leitura de informações da imagem.
      leitura >> tipo;
        setTipo(tipo);

      leitura.get();
        getline(leitura, comentario);

      leitura >> largura;
        setLargura(largura);

      leitura >> altura;
        setAltura(altura);

      leitura >> escala;
        setEscala(escala);
      leitura.ignore();
      //Alocação dinamica da memória para ponteiros duplos e triplos.
      pixels = (int**) malloc(largura * sizeof(int*));
      rgb = (int***) malloc(largura * sizeof(int**));

      for(i = 0; i < largura; i++){
        pixels[i] = (int*) malloc(altura * sizeof(int));
        rgb[i] = (int**) malloc(altura * sizeof(int*));
      }

      for(i = 0; i < largura; i++){
        for(j = 0; j < altura; j++){
            rgb[i][j] = (int*) malloc(3 * sizeof(int));
        }
      }
      //Leitura para quando o tipo da imagem for "P5"
      //Lendo caracteres binários
      if(strcmp(tipo, "P5") == 0){
        if(!leitura.eof()){
          char auxiliar;
          for(i = 0; i < largura; ++i){
            for(j = 0; j < altura; ++j){
              leitura.get(auxiliar);
              pixels[i][j] = int(auxiliar);
              setPixels(pixels);
            }
          }
          leitura.close();
        }
      comentarioPGM(nome_arquivo);
      }

      //Leitura para quando o tipo da imagem for "P2"
      //Lendo caracteres como inteiro
      if(strcmp(tipo, "P2") == 0){
        if(!leitura.eof()){
          for(i = 0; i < largura; ++i){
            for(j = 0; j < altura; ++j){
              leitura >> pixels[i][j];
              setPixels(pixels);
            }
          }
          leitura.close();
        }
      comentarioPGM(nome_arquivo);
      }

      //Leitura para quando o tipo da imagem for "P6"
      //Lendo caracteres binarios na imagem PPM
      if(strcmp(tipo, "P6") == 0){
        if(!leitura.eof()){
          char aux1;
          for(i = 0; i < largura; ++i){
            for(j = 0; j < altura; ++j){
              for(k = 0; k < 3; k++){
              leitura.get(aux1);
              unsigned char aux2 = (unsigned char)(aux1);

              rgb[i][j][k] = int(aux2);

              setRgb(rgb);
              }
            }
          }
          leitura.close();
        }
      comentarioPPM(nome_arquivo);
      }

      //Leitura para quando o tipo da imagem for "P3"
      //Lendo caracteres como inteiros na imagem PPM
      if(strcmp(tipo, "P3") == 0){

        if(!leitura.eof()){
          for(i = 0; i < largura; ++i){
            for(j = 0; j < altura; ++j){
              for(k = 0; k < 3; ++k){
                leitura >> rgb[i][j][k];
              }
            }
          }
          leitura.close();
        }
      comentarioPPM(nome_arquivo);
      }
      //Exclusão de ponteiros triplos e simples.
      for(i = 0; i < largura; i++){
        for(j = 0; j < altura; j++){
          delete[] rgb[i][j];
        }
      }
      for(i = 0; i < largura; i++){
        delete[] pixels[i];
        delete[] rgb[i];
      }
      delete[] rgb;
      delete[] pixels;

      rgb = NULL;
      pixels = NULL;
}

void Imagem::comentarioPGM(string nome_arquivo){
  //Declaração de variaveis
  int comeco;
  int tamanho;
  int shift;
  int i;
  int j;
  string tipo;
  string mensagem;
  int cont;
  int cont2;

  ifstream coment;
  coment.open(nome_arquivo);
  if(!coment.is_open()){
    coment.clear();
    throw(0);
  }
  getline(coment, tipo); //Ler a primeira linha referente ao tipo
  coment.get(); //Ler '#'

  //Lendo informações para descrifrar...
  coment >> comeco;
  coment >> tamanho;
  coment >> shift;

  coment.close();

  cont = 0;
  cont2 = 0;
  //Capturando a mensagem criptografada
    for(i = 0; i < getLargura(); ++i){
      for(j = 0; j < getAltura(); ++j){
        cont++;
        if(cont >= comeco && cont2 <= tamanho){
          mensagem = char(getPixels()[i][j]) + mensagem;
          cont2++;
          }
        }
      }
  //Caesar Cipher
      for(i = 0; i < tamanho; ++i){
        if(isalpha(mensagem[i])){
          for(j = 0; j < shift; ++j){
            if(mensagem[i] == 'a'){
              mensagem[i] = 'z';
            }
            else if(mensagem[i] == 'A'){
              mensagem[i] = 'Z';
            }
            else{
              mensagem[i]--;
          }
        }
      }
    }

    //Impressão da mensagem descriptografada no terminal
    //Isso foi feito pois a mensagem estava sendo escrita invertida pelo metodo utilizado no Caesar Cipher, que foi utilizado.
    cout << "A mensagem descriptografada é: ";
    for(i = tamanho - 1; i >= 0; --i){
      cout << mensagem[i];
    }
    cout << endl;
}

void Imagem::comentarioPPM(string nome_arquivo){
  //Declaração de variaveis
  int i;
  int j;
  int k;
  int comeco = 0;
  int tamanho = 0;
  string cifra = "";
  string tipo = "";
  string mensagem = "";
  int cont;
  int cont2;
  string novo_alfa = "";
  bool alf[26] = {0};
  int aux = 0;
  string alfabeto = " ABCDEFGHIJKLMNOPQRSTUVWXYZ";
  string mensagem1 = "";
  int length;
  int *posicoes;
  int n;
  int cont3;


  ifstream coment;
  coment.open(nome_arquivo);
  if(!coment.is_open()){
    coment.clear();
    throw(0);
  }

  getline(coment, tipo); //Ler o tipo da imagem
  coment.get(); //Ler o caracter de comentario '#'

  //Ler informações para descriptografia
  coment >> comeco;
  coment >> tamanho;
  coment >> cifra;

  //Colocando a palavra-chave declarada como "cifra" no alfabeto.
  cont = 0;
  cont2 = 0;
  length = cifra.size();
    for(i = 0; i < length; ++i){
      if(cifra[i] >= 'A' && cifra[i] <= 'Z'){
        if(alf[cifra[i] - 65] == 0){
          novo_alfa += cifra[i];
          alf[cifra[i] - 65] = 1;
        }
    }
    else if(cifra[i] >= 'a' && cifra[i] <= 'z'){
      if(alf[cifra[i] - 97] == 0){
        novo_alfa += cifra[i] - 32;
        alf[cifra[i] - 97] = 1;
      }
    }
  }

  for(i = 0; i < 26; i++){
    if(alf[i] == 0){
      alf[i] = 1;
      novo_alfa += char(i + 65);
    }
  }
  //Contadores para delimitar limites nas passagens pela imagem
  cont = 0;
  cont2 = 0;
  cont3 = 0;
  n = 0;

  posicoes = (int*) malloc(tamanho * sizeof(int));

  for(i = 0; i < getLargura(); ++i){
    for(j = 0; j < getAltura(); ++j){
      for(k = 0; k < 3; ++k){
        cont++;
        if(cont > comeco && cont3 < (tamanho * 3)){ //Define o começo da leitura da imagem e o termino sendo (tamanho * 3) sendo 3 o numero de variaveis por pixel.
          if(cont2 < 3){
            aux += (getRgb()[i][j][k] % 10); //Calculando e somando o módulo dos 3 numeros de inicio da mensagem.
            cont2++;
            if(cont2 == 3){ //Verificador de final da tríade RGB.
              cont2 = 0;
              posicoes[n] = aux; //Alocando a soma dos modulos para um vetor de inteiros
              n++;
              aux = 0; //Retornando variavel usada para calculo da soma dos modulos à 0.
            }
            cont3++; //Contando a quantidade de vezes que é somado um numero.
          }
        }
      }
    }
  }
  //Mapeando o alfabeto padrão. Que de acordo com um número de 0 - 26. Retorna uma letra, ou espaço em branco caso seja igual a 0;
  map<int, char> alfa;
  length = alfabeto.size();
  for(i = 0; i < length; ++i){
    alfa[i] = alfabeto[i];
  }
  //Mapeando o novo alfabeto, neste caso, o tem como indice um caracter, e retorno um inteiro.
  map<char, int> n_alfa;
  length = novo_alfa.size();
  for(i = 0; i < length; ++i){
    n_alfa[novo_alfa[i]] = i+1;
  }
  //Fazendo a transcrição de acordo com as posições tomadas no módulo para o alfabeto padrão.
  for(i = 0; i < tamanho; ++i){
    mensagem += alfa[posicoes[i]]; //Criação da mensagem criptografada.
  }

  //Keyword Cipher
  //O retorno dessa forma, sera sempre em caracteres maiusculos.
  //A passagem será feita de uma string criptografada para uma string descriptografada.
  length = mensagem.size();
  for(i = 0; i < length; ++i){
    if(mensagem[i] >= 'a' && mensagem[i] <= 'z'){
      int p = n_alfa[mensagem[i] - 32];
      mensagem1 += alfa[p];
    }
    else if(mensagem[i] >= 'A' && mensagem[i] <= 'Z'){
      int p = n_alfa[mensagem[i]];
      mensagem1 += alfa[p];
    }
    else{
      mensagem1 += mensagem[i];
    }
  }


  cout << "A mensagem descriptografada é: " << mensagem1 << endl; //Resultado final
}
