#include <iostream>
#include <fstream>
#include <string>
#include "imagem.hpp"
#include "pixel.hpp"
#include <stdlib.h>
#include <bits/stdc++.h>

using namespace std;

void image();

int main(int argc, char ** argv){

    cout << "Bem-vindo ao Programa de Descriptografia" << endl;
    cout << endl;
    cout << "Por favor, digite o nome da imagem que deseja descriptografar, inclua \".pgm\" ou \".ppm\"." << endl;
    cout << "Caso a imagem não esteja no diretório principal do programa, coloque o nome da pasta." << endl;
    cout << "Exemplo: pasta/imagem.ppm" << endl;
    cout << endl;
    cout << "Imagem: ";
    image();

    cout << endl;
    cout << "Feito por: Dâmaso Júnio Pereira Brasileo - Mat: 17/0031438" << endl;

  return 0;
}

void image(){
  Imagem * image = new Imagem();
  //Exception para caso o arquivo não abra.
  try{
    image->leituraImagem();
  }
  catch(int a){
    cout << "Não foi possivel encontrar o arquivo" << endl;
  }
  delete image;
}
